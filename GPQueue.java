/*@Author: Jason Morrow
 *Date Created: 03/30/2012
 *Edited: 04/01/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This class implements a generic Priority Queue.
 */

 //The priority should be built into the CompareTo method of T. The front of the queue has the Highest priority.
class GPQueue<T extends Comparable<T>> implements Scheduler<T>
{
	int next; //the index of the next available position in heap
	T[] heap;
	/*The root is at heap[0]
	 *children of heap[n] at heap[2n+1] and heap[2n+2]
	 *parent of heap[n] at heap[(n-1)/2]*/
	
	public GPQueue()
	{
		next = 0;
		heap = (T[]) new Comparable[32];
	}
	
	public boolean isEmpty()
	{
		return next==0;
	}
	
	//TODO: Make sure you always have a null row
	public void push(T newItem) //put something into the piority queue
	{
		if (heap.length < 2*next + 2) //This guarantees the presence of at least enough empty space for a row of null values
		{
			T[] temp = (T[]) new Comparable[heap.length*4];
			for (int i = 0; i < heap.length ; i++)
				temp[i] = heap[i];
			heap = temp;
		}
		
		int locus = next; //The index of where in the heap newItem is
		heap[next++] = newItem; //Stick the thing at the end
		//restore the heap condition
		while(heap[locus].compareTo(heap[(locus-1)/2]) > 0) //breaks out at bottom if locus ever reaches 0.
		{
			//swap newItem and its parent
			heap[locus] = heap[(locus - 1)/2];
			locus = (locus - 1)/2;
			heap[locus] = newItem; 
			
			if (locus == 0) break;
		}
	}
	
	//take the highest priority 
	public T pull()
	{
		T pocket = heap[0];
		heap[0] = heap[--next]; //note that it is necessary to forcibly remove this from the array.
		heap[next + 1] = null;
		int current = 0; //the location of the leaf that was stuck into the root
		T sinker = heap[0]; //the value of the leaf that was stuck into the root
		//sink what was the last leaf back down to its place
		boolean isHeap = false;
		while(!isHeap)
		{
			T right = heap[current*2 + 2];
			T left = heap[current*2 + 1];
			if (left != null)
			{
				if (sinker.compareTo(left) < 0)
				{
					heap[current] = left;
					current = current*2 + 1;
					heap[current] = sinker;
				}
				else if (right != null)
				{
					if (sinker.compareTo(right) < 0)
					{
						heap[current] = right;
						current = current*2 + 2;
						heap[current] = sinker;
					}
					else
						isHeap = true;
				}
				else
					isHeap = true;
			}
			else if (right != null)
			{
				if (sinker.compareTo(right) < 0)
				{
					heap[current] = right;
					current = current*2 + 2;
					heap[current] = sinker;
				}
				else
					isHeap = true;
			}
			else
				isHeap = true;
		}
		return pocket;
	}
}