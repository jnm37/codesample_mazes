/*@Author: Jason Morrow
 *Date Created: 03/30/2012
 *Edited: 03/31/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This class implements a generic Queue.
 */
 
class GQueue<T extends Comparable<T>> implements Scheduler<T>
{
	private class Node
	{
		public T value;
		public Node next;
		
		public Node(T v)
		{
			value = v;
			next = null;
		}
	}
	
	Node head;
	Node tail;
	
	public GQueue()
	{
		head = null;
		tail = null;
	}
	
	public boolean isEmpty()
	{
		return head == null;
	}
	
	public void push(T newVal) //add  to the back of the queue
	{
		if (head ==null)
		{
			head = new Node(newVal);
			tail = head;
		}
		else
		{
			tail.next = new Node(newVal);
			tail = tail.next;
		}
	}
	
	//take something from the front of the queue
	public T pull()
	{
		T pocket = head.value;
		head = head.next;
		return pocket;
	}
}