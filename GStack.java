/*@Author: Jason Morrow
 *Date Created: 03/27/2012
 *Edited: 04/04/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This class implements a generic stack.
 */

class GStack<T extends Comparable<T>> implements Scheduler<T>
{
	T[] mem;
	int next; //index of the slot in the array above the top
	
	public GStack()
	{
		next = 0;
		mem = (T[]) new Comparable[32];
	}
	
	public void push(T top)
	{
		if (next > mem.length)
		{
			T[] temp = (T[]) new Comparable[2*mem.length];
			for (int i = 0; i < mem.length; i++)
				temp[i] = mem[i];
			mem = temp;
		}
		mem[next++] = top;
	}
	
	public T pull()
	{
		return mem[--next]; //note that it is not necessary to take it out of the array
	}
	
	public boolean isEmpty()
	{
		return next==0;
	}
}