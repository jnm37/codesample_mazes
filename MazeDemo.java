/*@Author: Jason Morrow
 *Date Created: 04/04/2012
 *Edited: 04/04/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This performs and demonstrates the search algorithms mostly implemented in Mazesearch.java
 */
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.io.File;
 
class MazeDemo
{
	MazeSquare[][] maze;
	MazeSearch[] search;
	
	JFrame[] window;
	MazePanel[] panel;
	
	private class MazePanel extends JPanel
	{
		int boxSize; //the side length of each element of the panel
		MazeSquare[][] maze;
		MazeSquare[] visited;
		
		public MazePanel(MazeSquare[][] m)
		{
			boxSize=20;
			maze = m;
			visited = new MazeSquare[0];
			setPreferredSize(new Dimension(m.length*boxSize, m[0].length*boxSize));
		}
		
		public void update(MazeSquare[] v)
		{
			visited = v;
			repaint();
		}
		
		public void drawBox(int c, int r, Color color, Graphics g)
		{
			int cornerX = c*boxSize;
			int cornerY = (maze[0].length - r - 1)*boxSize;
			g.setColor(color);
			g.fillRect(cornerX, cornerY, boxSize, boxSize);
		}
		
		public void drawDot(int c, int r, Color color, Graphics g)
		{
			int cornerX = c*boxSize;
			int cornerY = (maze[0].length - r - 1)*boxSize;
			g.setColor(color);
			g.fillOval(cornerX, cornerY, boxSize, boxSize);
		}
		
		public void paintComponent(Graphics g)
		{
			for (int i = 0; i < maze.length; i++) //draw each square
				for (int j = 0; j < maze[0].length; j++)
				{
					if (maze[i][j].type() == MazeSquare.OPEN)
						drawBox(i, j, Color.WHITE, g);
					else if (maze[i][j].type() == MazeSquare.BLOCKED)
						drawBox(i, j, Color.BLACK, g);
					else if (maze[i][j].type() == MazeSquare.START) //green for go at the start
						drawBox(i, j, Color.GREEN, g);
					else if (maze[i][j].type() == MazeSquare.GOAL) //red for stop at the end
						drawBox(i, j, Color.RED, g);
				}
			for (int k = 0; k < visited.length; k++)
				drawDot(visited[k].getX(), visited[k].getY(), Color.YELLOW, g);
		}
	}
	
	public MazeDemo(MazeSquare[][] m, MazeSquare s)
	{
		maze = m;
		search = new MazeSearch[3]; window = new JFrame[3]; panel = new MazePanel[3];
		
		search[0] = new MazeSearch(maze, s, MazeSearch.DEPTH);
		search[1] = new MazeSearch(maze, s, MazeSearch.BREADTH);
		search[2] = new MazeSearch(maze, s, MazeSearch.BEST);
		
		window[0] = new JFrame("DEPTH FIRST"); panel[0] = new MazePanel(maze); window[0].getContentPane().add(panel[0]);
		window[1] = new JFrame("BREADTH FIRST"); panel[1] = new MazePanel(maze); window[1].getContentPane().add(panel[1]);
		window[2] = new JFrame("BEST FIRST"); panel[2] = new MazePanel(maze); window[2].getContentPane().add(panel[2]); window[2].setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		for(JFrame i : window)
		{
			i.setVisible(true);
			i.pack();
		}
			
		for (int i=0; i<3; i++)
		{
			sleep(9000);
			while(! search[i].step())
			{
				panel[i].update(search[i].visited());
				sleep(600);
			}
			panel[i].update(search[i].visited());
		}
	}
	
	public void sleep(long milliseconds)
	{
		Date d = new Date();
		long start, now;
		start = d.getTime();
		do
		{
			d = new Date();
			now = d.getTime();
		} while ( now - start < milliseconds);
	}
	
	//create the maze, feed it to the constructor
	public static void main(String[] cmd) throws java.io.FileNotFoundException
	{
		File f = null;
		try
		{
			f = new File(cmd[0]);
		}
		catch (Exception e)
		{
			System.out.println("You need to enter a file name in the command line!");
			System.exit(5);
		}
		Scanner fScan = new Scanner(f);
		ArrayList<String> fileLines = new ArrayList<String>();
		while (fScan.hasNext())
			fileLines.add(fScan.nextLine());
			
		int x = fileLines.get(0).toCharArray().length; //the width of the maze
		int y = fileLines.size(); //the height of the array
		MazeSquare[][] m = new MazeSquare[x][y];
		MazeSquare s = null;
		MazeSquare g = null;
		for (int b = 0; b < y; b++)
		{
			char[] row = fileLines.get(b).toCharArray();
			for (int a = 0; a < x; a++)
			{
				m[a][y-1-b] = new MazeSquare(row[a], a, y-1-b);
				if (row[a] == MazeSquare.START) s = m[a][y-1-b];
				if (row[a] == MazeSquare.GOAL) g = m[a][y-1-b];
			}
		}
		
		//now set the distances
		x = g.getX(); //reuse this variable for the position of the goal, so distances can be calculated
		y = g.getY();
		for (int u = 0; u < m.length; u++)
			for (int v = 0; v < m[0].length; v++)
				m[u][v].setDistance(Math.abs(u-x)+Math.abs(v-y));
		
		new MazeDemo(m, s);
	}
}