/*@Author: Jason Morrow
 *Date Created: 04/01/2012
 *Edited: 04/04/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This class implements search algorithms
 */
import java.util.ArrayList;

class MazeSearch
{
	public static int DEPTH = 1;
	public static int BREADTH = 2;
	public static int BEST = 3;
	private int type; //which of the above algorithms is to be used
	
	private MazeSquare[][] m; //the maze except with null values at visited or scheduled squares
	private ArrayList<MazeSquare> visited;
	private Scheduler<MazeSquare> path; //The structure that determines what will be tested next
	//The entire maze should be the almost disjoint union of m, visited, and path
	
	//m is the maze. s is the start. algorithm determines which algorithm is to be used.
	public MazeSearch(MazeSquare[][] maze, MazeSquare s, int algorithm)
	{
		m = new MazeSquare[maze.length][maze[0].length]; //copy maze so that the search is nondestructive
		for(int i = 0; i < maze.length; i++)
			for(int j = 0; j < maze[0].length; j++)
				m[i][j] = maze[i][j];
		
		if (algorithm == DEPTH)
			path = new GStack<MazeSquare>();
		else if (algorithm == BREADTH)
			path = new GQueue<MazeSquare>();
		else //Assume this is Best First
			path = new GPQueue<MazeSquare>();
		path.push(s);
		m[s.getX()][s.getY()] = null;
		type = algorithm;
		visited = new ArrayList<MazeSquare>();
	}
	
	//Do a bit of searching. Return true if the goal is found or if the entire maze has been searched.
	public boolean step()
	{
		if (path.isEmpty()) return true;
		MazeSquare next = path.pull();
		int x = next.getX();
		int y = next.getY();
		visited.add(next);
		
		//schedule the dealing with the square to the north of next
		if (y + 1 < m[0].length) //Do this only if the square exists
		{
			if (m[x][y+1]!=null) //Do this only if the square has not already been scheduled
			{
				if(m[x][y+1].type() == MazeSquare.GOAL) //The Goal has been found. The search is complete
				{
					visited.add(m[x][y+1]);
					return true;
				}
				else if(m[x][y+1].type() == MazeSquare.OPEN)
				{
					path.push(m[x][y+1]);
					m[x][y+1] = null;
				}
			}
		}
		
		//Do the same for the square to the south of next
		if(y > 0)
		{
			if(m[x][y-1] != null)
			{
				if(m[x][y-1].type() == MazeSquare.GOAL)
				{
					visited.add(m[x][y-1]);
					return true;
				}
				else if(m[x][y-1].type() == MazeSquare.OPEN)
				{
					path.push(m[x][y-1]);
					m[x][y-1] = null;
				}
			}
		}
		
		//Do the same for the square to the east of next
		if (x+1 < m.length)
		{
			if(m[x+1][y] != null)
			{
				if(m[x+1][y].type() == MazeSquare.GOAL)
				{
					visited.add(m[x+1][y]);
					return true;
				}
				else if(m[x+1][y].type() == MazeSquare.OPEN)
				{
					path.push(m[x+1][y]);
					m[x+1][y] = null;
				}
			}
		}
		
		//Do the same for the square to the west of next
		if (x > 0)
		{
			if(m[x-1][y] != null)
			{
				if(m[x-1][y].type() == MazeSquare.GOAL)
				{
					visited.add(m[x-1][y]);
					return true;
				}
				else if(m[x-1][y].type() == MazeSquare.OPEN)
				{
					path.push(m[x-1][y]);
					m[x-1][y] = null;
				}
			}
		}
		
		return false;
	}
	
	public MazeSquare[] visited()
	{
		return visited.toArray(new MazeSquare[visited.size()]);
	}
}