/*@Author: Jason Morrow
 *Date Created: 04/01/2012
 *Edited: 04/04/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This class represents an element of a maze to be searched
 */
 
class MazeSquare implements Comparable<MazeSquare>
{
	public static char OPEN = ' ';
	public static char BLOCKED = '*';
	public static char START = 'S';
	public static char GOAL = 'G';
	
	private int distance; //Manhattan distance from this square to the goal. Used in Best First
	private char type; //whether this square is open, blocked, start, or goal
	private int x; //The horizontal position of this square
	private int y; //The vertical position of this square
	//I use the convention that (0,0) is the bottom left corner
	
	//t is the type, a is the horizontal position, b is the vertical position
	public MazeSquare(char t, int a, int b) throws IllegalArgumentException
	{
		if( t != OPEN && t != BLOCKED && t != START && t != GOAL)
			throw new IllegalArgumentException(t + " is not a valid maze square contents value.");
		type = t;
		distance = -1;
		x = a;
		y = b;
	}
	
	//return positive if this is closer, 0 if this is equidistant, negative if this is farther
	public int compareTo(MazeSquare s)
	{
		return s.distance - this.distance;
	}
	
	//return whether the two squares have the same position
	public boolean equals(MazeSquare s)
	{
		return x==s.getX() && y==s.getY();
	}
	
	public void setDistance(int d)
	{
		distance = d;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public char type()
	{
		return type;
	}
}