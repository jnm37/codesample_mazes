This program demonstrates searching algorithms on a maze. Each element of the maze is represented as a MazeSquare, which contains its location in the maze, type, and distance from the goal. MazeSearch uses a Scheduler (stack, queue, or priority queue) to schedule the testing of MazeSquares for goalness. To avoid going around in circles, it removes the squares from its internal maze when they are scheduled. MazeDemo contains all the GUI, file IO, and instructions for MazeSearch to perform steps of its search. An array of visited tiles is passed from MazeSearch to MazeDemo. These appear as yellow dots.
My GUI looks different from Aronis's, and I don't highlight the last square to be visited. I may also put my squares into my schedulers in different order. However, I do implement the proper Depth-first, Breadth-first, and Best-first algorithms.

The program is contained in: MazeSearch.java, GStack.java, GQueue.java, GPQueue.java, MazeSquare.java, Scheduler.java, and MazeDemo.java
I have also included a maze file: maze1.txt

To compile the code, simply use "javac MazeDemo.java".
To run the program use "java MazeDemo maze1.txt" or use a different maze file as the command-line argument.

No problems with the program are known.