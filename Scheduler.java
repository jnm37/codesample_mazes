/*@Author: Jason Morrow
 *Date Created: 04/03/2012
 *Edited: 04/03/2012
 *Univesity of Pittsburgh CS 0445
 *Project 4: Search Algorithms
 *This interface provides a way to access a stack, queue, or priority queue in the same way
 */

interface Scheduler<T extends Comparable<T>>
{
	public void push(T item);
	
	public T pull();
	
	public boolean isEmpty();
}